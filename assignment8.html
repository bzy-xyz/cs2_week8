<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link href="cs2_assignment.css" rel="stylesheet" type="text/css" />
<title>CS2 Assignment 8: Numerics</title>
</head>

<body>
<div class="content">

<div class="author">Author: Ellen Price</div>
<h1>CS2 Assignment 8: Numerics</h1>
<h2>Due Tuesday, March 5, 2013, at 4:00 AM</h2>

<hr />

<h2>Introduction</h2>

<p>This is the CS2 assignment on numerics. In particular, you will be
implementing a Fast Fourier Transform and three numeric integrators.</p>

<p>When finished, please enclose your submission as a ZIP file named 
<span class="code">cs2week8-[NAME].zip</span> or a tar.gz file named 
<span class="code">cs2week8-[NAME].tar.gz</span>, and upload it to the 
Week 8 assignment module on Moodle.</p>

<h2>Assignment Background</h2>

<p>Today, we use computers to do a <b>lot</b> of numerical computations,
particularly in the sciences; whether you use a graphing calculator
or <i>Mathematica</i>, you almost certainly use some kind of
computerized system for doing calculations you would rather not do by
hand. Sometimes, we need computers to perform calculations that
we cannot do on paper. In this assignment, you will explore two
canonical examples of numerical methods with C++: the Fast Fourier
Transform and numeric solutions of ordinary differential equations.</p>

<h3>Fast Fourier Transform</h3>

<p>I will summarize Fourier's theorem here without proof. Given
any periodic function <i>f(x)</i>, we can approximate <i>f(x)</i>
by</p>

<center><img src="images/FourierApprox.gif" /></center>

<p>As <i>n</i> approaches infinity, we approach the function <i>f</i>
exactly:</p>

<center><img src="images/FourierExact.gif" /></center>

<p>The image below may be familiar to you; it shows discrete Fourier
series approximations of a square wave. As the order of the approximation
increases, it approaches the true shape of the function. (If you would
like to learn more about Fourier's theorem,
<a href="http://press.princeton.edu/books/maor/chapter_15.pdf">this document</a>
has some very good information, along with very good diagrams.)</p>

<center><img src="images/FourierTransforms.gif" /></center>

<p>That seems like a really cool result, if we had an exact functional
form for <i>f</i>. If we just have empirical data (which might have
errors!), there's a problem. The Fast Fourier Transform provides
a way around that issue by transforming an arbitrary data array
from the time domain to the frequency domain (where you can imagine
"domain" to mean "the x-axis"). Thus, the result of the FFT is
intensity as a function of frequency instead of as a function of
time. This is incredibly useful if you want to know what components
make up a complex signal, which is why the FFT is used so liberally
throughout signal analysis and processing. You will be asked to implement
the FFT only; we do not expect you to prove any theorems.</p>

<h3>Numerically solving differential equations</h3>

<p>Differential equations are ubiquitous throughout the sciences,
but not all differential equations have closed-form solutions; even
so, we might still like to get some useful information from them.
What we cannot solve analytically, we <i>can</i> solve numerically.
Three common algorithms of numerically integrating differential
equations are the forward, backward, and symplectic Euler methods.</p>

<p>All three of the Euler methods follow a similar pattern: Figure
out the next state by updating the current state according to some
rule. The update rules are defined differently for each method,
however. Consider some system with a position <i>x</i> and
velocity <i>v</i>; we have only the functional form of the
acceleration, <i>f(x, t) = dv/dt</i>. If we use the forward Euler method,
then the position <i>x</i> after <i>n</i> timesteps will be given
by</p>

<center><img src="images/ForwardEuler.gif" /></center>

<p>where delta is a small timestep. Thus, we find out the next position
and velocity by examining only the current position and velocity. Note
that we can find them in either order, since the new velocity does not
depend on the new position.</p>

<p>How might we go about this differently? We could do something really
extreme and base the next step on the next step; don't panic yet, though,
because it's not that bad. The update rule becomes</p>

<center><img src="images/BackwardEuler.gif" /></center>

<p>After seeing the equations, maybe you've realized what to do. If we
substitute the first equation into the second one, we get an equation in
terms of the new position, old position, and old velocity (one unknown).
After solving this equation, we can find the new position and thus the
new velocity. This is the backward Euler.</p>

<p>We have one left - symplectic Euler. It is actually very similar to
the forward Euler, and its update rule is given below.</p>

<center><img src="images/SymplecticEuler.gif" /></center>

<p>As you can see, we make only one change from forward Euler: We use
the new velocity to calculate the new position instead of using the
current velocity. All of the Euler methods behave somewhat differently
after many timesteps, an effect that you will observe in this week's
assignment.</p>

<h2>Prerequisites</h2>

<p><ul>
    <li>g++ 4.6.x+</li>
    <li>liballegro4.2-dev</li>
    <li>libldpng</li>
    <li>libpng1.2.49</li>
    <li>gnuplot 4.6</li>
</ul>

You can download libloadpng <b>for a personal computer</b> from
<a href='http://tjaden.strangesoft.net/loadpng/'>its home page</a> and
install by running <span class='code'>make install</span> at a
terminal prompt. Ask a TA if you need help retrieving these packages,
or if these packages appear to be missing from the CS cluster.</p>

<h2>Assignment (20 points)</h2>

<p>For this assignment, you will implement a Fast Fourier Transform
(FFT) algorithm to analyze audio signals and a numeric integrator to
visualize planetary orbits.</p>

<p><b>IMPORTANT:</b> If you are working in the Annenberg lab, please
modify the provided Makefile by replacing the lines</p>

<div class="code">
CFLAGS = -Wall -ansi -ggdb `libpng-config --I_opts` -ggdb
LIBS = -lz -lm -lldpng `libpng-config --L_opts` -lpng `allegro-config --libs`
</div>

<p>with</p>

<div class="code">
CFLAGS = -Wall -ansi -ggdb `libpng-config --I_opts` -I/cs/courses/cs2/allegro-4.4.2/include -I/cs/courses/cs2/allegro-4.4.2/build/include -I/cs/courses/cs2/loadpng
LIBS = -lz -lm `libpng-config --L_opts` -lpng -L/cs/courses/cs2/allegro-4.4.2/build/lib -lalleg -L/cs/courses/cs2/loadpng -lldpng
</div>

<p class="hilite">and run
<span class="code">export LD_LIBRARY_PATH=/cs/courses/cs2/allegro-4.4.2/build/lib:$LD_LIBRARY_PATH</span>
once per session (you need to run it again if you log out and log
back in).</p>

<h3>Fast Fourier Transform</h3>

<p><div class="points easy">1</div>First, we want to make sure you
conceptually understand the FFT. Consider the simple case of an
input that is a pure sine wave. What should the Fourier transform
(not the FFT!) look like? How might the FFT of the same signal look
different, keeping in mind that it is discrete? What would you
reasonably expect to happen as the function becomes more and more
complicated? Place your responses to these questions in a file
called <span class="code">README.txt</span> in the base directory
of your assignment.</p>

<p><div class="points easy">4</div>As previously mentioned, we are not
asking you to derive the FFT algorithm; it is well-known and
available pre-packaged from almost any good numerics library.
However, just so that you know how it works, we are asking you
to translate it from another language (Fortran 77). As boring as this
might seem, being able to translate code from different languages
is an important skill that you might need, particularly in certain
branches of science (consider astronomy, notorious for its use of
Fortran and IDL, or statistics, which uses S and its open-source
equivalent R).</p>

<p>Go ahead and open SignalAnalyzerGameState.cpp. You will not need
to worry about most of the functions; several are related to the
GUI. However, you will need to translate the Fortran function below
(<span class="code">four1</span>) to C++ and put your code in the
<span class="code">fft</span> function. You can see from the function
signature that these functions take exactly the same arguments.
<b>This Fortran subroutine is not owned by me, CS2, or Caltech; it is
copyright 1986-1992 <i>Numerical Recipes in Fortran 77: The Art of
Scientific Computing</i> published by Cambridge University Press.</b>
Small adjustments have been made by myself and Ben Yuan.</p>

<p>To make your life a bit easier, here are some helpful hints for
converting this code to C++:</p>

<ol>
    <li>You'll notice that Fortran arrays are 1-based, while C++ arrays
        are usually 0-based. <b>This is okay!</b> The signal analyzer
        program expects a 1-based C++ array and allocates enough memory
        for one; you should not have to change any array indices.</li>
    <li>While we're looking at Fortran arrays, you'll notice that
        Fortran uses parentheses () instead of square brackets [] to
        index array elements.</li>
    <li>You may notice some loops that look suspiciously like
        <span class="code">for</span> loops; in Fortran, they are
        <span class="code">do</span> loops. In the
        <span class="code">do</span> statement, the first number
        is the starting value for the iteration variable (usually 
        <span class="code">i</span>), the second number is the ending
        value, and the third number is the step size.</li>
    <li>Speaking of suspicious loops, you may not catch that an
        <span class="code">if...goto...endif</span> statement
        is actually a loop, but it is. The command
        <span class="code">goto 1</span> does not mean to go to the first
        line; rather, it means to go to the line prefixed by 1 (the
        <span class="code">if</span> statement, in this case). These loops
        are easily replaced with C++ <span class="code">while</span>
        loops.</li>
    <li>The Fortran single-line comment character is "!" (equivalent
        to C++ "//").</li>
    <li>A Fortran variable declared as <span class="code">REAL</span>
        is the same as a C++ <span class="code">float</span>; a
        Fortran <span class="code">DOUBLE PRECISION</span> is the
        same as a C++ <span class="code">double</span>.</li>
    <li>In Fortran, comparisons are very different.
        <span class="code">.gt.</span> is equivalent to "greater than,"
        etc.</li>
</ol>

<p><b>Since this is strictly a translation question, consulting any
other <i>Numerical Recipes</i> besides the Fortran 77 version
copyright 1986-1992 would be a violation of the honor code.</b></p>

<div class="code">
SUBROUTINE four1(data, nn, isign)
    INTEGER isign, nn
    REAL data(2*nn)
    ! Replaces data(1:2*nn)by its discrete Fourier transform, if isign
    ! is input as 1; or replaces data(1:2*nn) by nn times its inverse
    ! discrete Fourier transform, if isign is input as −1. data is a
    ! complex array of length nn or, equivalently, a real array of
    ! length 2*nn. nn MUST be an integer power of 2 (this is not checked
    ! for!).
    INTEGER i, istep, j, m, mmax, n
    REAL tempi, tempr, nn_inverse
    ! Double precision for the trigonometric recurrences.
    DOUBLE PRECISION theta, wi, wpi, wpr, wr, wtemp

    n = 2 * nn
    j = 1
    nn_inverse = 1.d0 / nn

    do i = 1, n, 2    ! This is the bit-reversal section of the routine.
        if (j.gt.i) then
            tempr = data(j)    ! Exchange the two complex numbers.
            tempi = data(j+1)
            data(j) = data(i)
            data(j+1) = data(i+1)
            data(i) = tempr
            data(i+1) = tempi
        endif

        m = n / 2

        1 if ((m.ge.2).and.(j.gt.m)) then
            j = j - m
            m = m / 2
            goto 1
        endif

        j = j + m
    enddo

    mmax = 2    ! Here begins the Danielson-Lanczos section of the routine.

    2 if (n.gt.mmax) then ! Outer loop executed log2 nn times.
        istep = 2 * mmax
        ! Initialize for the trigonometric recurrence.
        theta = 6.28318530717959d0 / (isign * mmax)
        wpr = -2.d0 * sin(0.5d0 * theta)**2
        wpi = sin(theta)
        wr = 1.d0
        wi = 0.d0

        do m = 1, mmax, 2    ! Here are the two nested inner loops.
            do i = m, n, istep
                j = i + mmax    ! This is the Danielson-Lanczos formula.
                tempr = sngl(wr) * data(j) - sngl(wi) * data(j+1)
                tempi = sngl(wr) * data(j+1) + sngl(wi) * data(j)
                data(j) = data(i) - tempr
                data(j+1) = data(i+1) - tempi
                data(i) = data(i) + tempr
                data(i+1) = data(i+1) + tempi
            enddo

            wtemp = wr    ! Trigonometric recurrence.
            wr = wr * wpr - wi * wpi + wr
            wi = wi * wpr + wtemp * wpi + wi
        enddo

        mmax = istep
        goto 2    ! Not yet done.
    endif    ! All done.

    if (isign.eq.-1)
        do i = 1, n, 1
            data(i) = data(i) * nn_inverse
        enddo
    endif
return
END
</div>

<p> After writing this function, you should be able to run
<span class="code">./Numerics</span> from terminal, select the
Signal Analyzer, open one of the provided *.wav files, and view its
FFT and inverse FFT by clicking "FFT" and "IFFT", respectively.</p>

<p><div class="points easy">3</div>Now, let's do something fun with
our FFT data. You may have noticed a "Filter" button when you ran
the assignment to test your FFT. You should write a highpass filter
function in <span class="code">highpass_filter</span>. A highpass
filter should allow high frequencies through and reduce low
frequencies (hopefully, you can see that doing this on FFT data is
actually a very straightforward exercise). I chose a sigmoid function
with the form below for this purpose.</p>

<center><img src="images/HighpassFunction.gif" /></center>

<p>In this equation, <i>x</i> is an FFT data point and <i>y</i> is an
output value; you should store your function's output in the
input array <span class="code">out</span>. <i>N</i> is the total
number of samples stored in the array, conveniently provided for you
in the macro <span class="code">NUM_SAMPLES</span>; since all arrays
are 1-based in this code, you should loop from 1 to
<span class="code">NUM_SAMPLES</span> when looping over the data array
for this part.</p>

<p>Keep in mind that, while the FFT outputs real and imaginary data
in alternating array elements, you should perform the filter function
on all of the elements (not just the real ones). This is because we
care about the magnitude of each complex number returned, not just
its real part.</p>

<p><div class="points hard">2</div>You may have noticed that your
waveforms do not fit exactly in the window provided by the signal
analyzer program, so they are not exactly periodic on that scale.
But the FFT expects a periodic function! Write the function
<span class="code">window</span> so that it applies some window
function to the raw data (Wikipedia provides
<a href="http://en.wikipedia.org/wiki/Window_function">a rather
exhaustive list</a> of nice window functions). It should make your
data periodic with respect to the time range we examine with the
signal analyzer. In your <span class="code">README.txt</span>, tell
what function you chose and what differences in the FFT spectrum
you observed after implementing it.</p>

<h3>Euler methods and orbits</h3>

<p>In the Assignment Background, I gave the equations for the three
Euler methods; now, you will write them for a specific physical
system, a planet orbiting a star. Open OrbitsGameState.cpp; the
functions you will write are <span class="code">forward_euler</span>,
<span class="code">backward_euler</span>, and
<span class="code">symplectic_euler</span>. You'll notice that none
of those functions return anything because they modify private
class variables: <span class="code">x, y</span> for position and
<span class="code">xvel, yvel</span> for velocity. Though this
problem is 2-dimensional, remember from Newtonian physics that you
can treat the dimensions as independent.</p>

<p>For eccentric orbits, we'll use the equations below to get the
acceleration:</p>

<center><img src="images/Orbits.gif" /></center>

<p><div class="points easy">3</div>Implement the forward Euler method
in <span class="code">forward_euler</span>. You will need to update
the position and velocity every time this function is called.</p>

<p><div class="points easy">3</div>Implement the symplectic Euler method
in <span class="code">symplectic_euler</span>. You will need to update
the position and velocity every time this function is called.</p>

<p><div class="points hard">3</div>For some systems, solving the
simultaneous equations to get equations for backward Euler is relatively
simple; unfortunately, this is not the case for the elliptic orbit
equations. Instead of asking you to find a way to do this, we will
instead ask you to make an assumption that will make the problem easier:
If the eccentricity of the orbit is 0 (i.e. the orbit is circular), then
the denominator in the acceleration function is a constant; it is
equal to the cube of the radius of the circle. Furthermore, for our
system, the radius is 1. With this in mind, implement the backward
Euler method in <span class="code">backward_euler</span>, using the
simplifying assumption described above.</p>

<p>To test any of these methods run <span class="code">./Numerics</span>
from the command line and select the Orbits menu item. You can use
'b' to select the backward Euler, 'f' to select the forward Euler, and
's' to select the symplectic Euler; to quit at any time, use 'q'.</p>

<p><div class="points easy">1</div>Observe your orbiting systems for
a while (this shouldn't actually take that long) so that you can see
their long-term behavior. You should observe three distinct end cases
for the three methods. In the same <span class="code">README.txt</span>
from the first part of the assignment, describe the behaviors of the
three methods in your own words. Which one produces the most accurate
long-term behavior? You can use the energy value given in the upper
left corner of the screen to help you; remember that energy should
be (roughly) constant because we are approximating a bound system.</p>

<p>If you have any questions about this week's assignment, please contact 
cs2-tas@ugcs.caltech.edu, or show up at any TA's office hours.</p>

</div>
</body>
</html>
