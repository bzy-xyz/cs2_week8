CC = g++
CFLAGS = -Wall -ansi -ggdb `libpng-config --I_opts`
LIBS = -lz -lm -lldpng `libpng-config --L_opts` -lpng `allegro-config --libs`

OBJS = main.o GameState.o MainMenuGameState.o SignalAnalyzerGameState.o OrbitsGameState.o

all: Numerics

Numerics: $(OBJS)
	$(CC) $(OBJS) $(LIBS) -o Numerics

main.o: main.cpp main.h
	$(CC) $(CFLAGS) -c main.cpp
	
GameState.o: GameState.cpp GameState.h
	$(CC) $(CFLAGS) -c GameState.cpp

MainMenuGameState.o: MainMenuGameState.cpp MainMenuGameState.h SignalAnalyzerGameState.h OrbitsGameState.h
	$(CC) $(CFLAGS) -c MainMenuGameState.cpp
	
SignalAnalyzerGameState.o: SignalAnalyzerGameState.cpp SignalAnalyzerGameState.h
	$(CC) $(CFLAGS) -c SignalAnalyzerGameState.cpp
	
OrbitsGameState.o: OrbitsGameState.cpp OrbitsGameState.h
	$(CC) $(CFLAGS) -c OrbitsGameState.cpp
	
clean:
	rm -f Numerics *.o plot.data plot.png
