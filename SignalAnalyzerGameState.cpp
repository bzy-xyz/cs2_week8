/****************************************************************************
*
* SignalAnalyzerGameState.cpp
* State that renders the signal analyzer.
*
* This code copyright (c) 2012 Ellen Price <eprice@caltech.edu>
* 
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 3. The name of the author may not be used to endorse or promote products
*    derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
* NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
* THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
****************************************************************************/

#include "SignalAnalyzerGameState.h"

#define PHYSICS_DELAY   (48)
#define RENDER_DELAY    (16)

#define MENU_LENGTH     (9)
#define BUTTON_WIDTH    (100)
#define BUTTON_HEIGHT   (40)
#define BUTTON_SPACE    (50)
#define BUTTON_Y        (SCREEN_H - 30 - BUTTON_HEIGHT)

#define STATUS_X        (5)
#define STATUS_Y        (SCREEN_H - 20)

#define BITMAP_W        (640)
#define BITMAP_H        (480)
#define BITMAP_X        ((SCREEN_W - BITMAP_W) / 2)
#define BITMAP_Y        (10)

using namespace std;

extern GameState *top_state;

/* 
 * Initializes the GameState.
 */ 
SignalAnalyzerGameState::SignalAnalyzerGameState()
{
    /* Create the menu */
    dlg = (DIALOG *) calloc(MENU_LENGTH, sizeof(DIALOG));
    construct_menu();
    
    /* Enable PNG loading */
    register_bitmap_file_type("png", load_png, save_png);
    
    /* Initialize the status message */
    strcpy(status, "Status: Signal Analyzer running");
    
    /* Initialize bitmap */
    bmp = NULL;
    
    /* Allocate memory */
    d = (float *) calloc(NUM_SAMPLES + 1, sizeof(float));
    sig = (float *) calloc(NUM_SAMPLES + 1, sizeof(float));
    
    /* Initialize state */
    contents = CONTENTS_NONE;
}

/* 
 * Deinitializes the GameState.
 */
SignalAnalyzerGameState::~SignalAnalyzerGameState()
{
    /* Free allocated resources. */
    free(dlg);
    destroy_bitmap(bmp);
}

void SignalAnalyzerGameState::do_physics_proc()
{
    ((SignalAnalyzerGameState *) top_state)->physics_step();
}

void SignalAnalyzerGameState::do_render_proc()
{
    ((SignalAnalyzerGameState *) top_state)->render();
}

/* 
 * Runs the GameState.
 */
StateStackCommand *SignalAnalyzerGameState::run()
{   
    int ret;
    StateStackCommand *retc = new StateStackCommand();
    
    render();
    ret = do_dialog(dlg, -1);
    
    switch(ret)
    {
        default:
            retc->cmd = POP;
            retc->new_state = NULL;
    }
    
    return retc;    
}

/* 
 * Runs a step of the physics layer.
 */
void SignalAnalyzerGameState::physics_step()
{
    
}

/* 
 * Processes incoming input, making it available to the physics layer.
 */
void SignalAnalyzerGameState::process_input()
{
    
}

/* 
 * Renders everything.
 */
void SignalAnalyzerGameState::render()
{
    start_render();    
    clear_to_color(doublebuf, makecol(255,255,255));
    
    if (bmp)
        blit(bmp, this->doublebuf, 0, 0, this->buf_xorig + BITMAP_X, 
            this->buf_yorig + BITMAP_Y, BITMAP_W, BITMAP_H);
    
    end_render();
    
    object_message(&dlg[1], MSG_DRAW, 0);
    object_message(&dlg[2], MSG_DRAW, 0);
    object_message(&dlg[3], MSG_DRAW, 0);
    object_message(&dlg[4], MSG_DRAW, 0);
    object_message(&dlg[5], MSG_DRAW, 0);
    object_message(&dlg[6], MSG_DRAW, 0);
}

/*
 * Initializes the menu at the bottom of the screen.
 */
void SignalAnalyzerGameState::construct_menu()
{
    DIALOG *t;
    int n = 0;
    
    t = &dlg[n];
    t->proc = d_clear_proc;
    n++;
    
    t = &dlg[n];
    t->proc = d_text_proc;
    t->x = STATUS_X;
    t->y = STATUS_Y;
    t->dp = status;
    n++;
    
    t = &dlg[n];
    t->proc = mem_fun_button_proc;
    t->x = SCREEN_W / 2 - 2.5 * BUTTON_WIDTH - 2 * BUTTON_SPACE;
    t->y = BUTTON_Y;
    t->w = BUTTON_WIDTH;
    t->h = BUTTON_HEIGHT;
    t->flags = D_EXIT;
    t->dp = (void *) "Load WAV";
    t->dp2 = (void *) this;
    t->dp3 = (void *) &SignalAnalyzerGameState::read_wav;
    n++;
    
    t = &dlg[n];
    t->proc = mem_fun_button_proc;
    t->x = SCREEN_W / 2 - 1.5 * BUTTON_WIDTH - BUTTON_SPACE;
    t->y = BUTTON_Y;
    t->w = BUTTON_WIDTH;
    t->h = BUTTON_HEIGHT;
    t->flags = D_EXIT;
    t->dp = (void *) "View Signal";
    t->dp2 = (void *) this;
    t->dp3 = (void *) &SignalAnalyzerGameState::draw_signal;
    n++;
    
    t = &dlg[n];
    t->proc = mem_fun_button_proc;
    t->x = SCREEN_W / 2 - 0.5 * BUTTON_WIDTH;
    t->y = BUTTON_Y;
    t->w = BUTTON_WIDTH;
    t->h = BUTTON_HEIGHT;
    t->flags = D_EXIT;
    t->dp = (void *) "View FFT";
    t->dp2 = (void *) this;
    t->dp3 = (void *) &SignalAnalyzerGameState::draw_fft;
    n++;
    
    t = &dlg[n];
    t->proc = mem_fun_button_proc;
    t->x = SCREEN_W / 2 + 0.5 * BUTTON_WIDTH + BUTTON_SPACE;
    t->y = BUTTON_Y;
    t->w = BUTTON_WIDTH;
    t->h = BUTTON_HEIGHT;
    t->flags = D_EXIT;
    t->dp = (void *) "Filter";
    t->dp2 = (void *) this;
    t->dp3 = (void *) &SignalAnalyzerGameState::highpass;
    n++;
    
    t = &dlg[n];
    t->proc = mem_fun_button_proc;
    t->x = SCREEN_W / 2 + 1.5 * BUTTON_WIDTH + 2 * BUTTON_SPACE;
    t->y = BUTTON_Y;
    t->w = BUTTON_WIDTH;
    t->h = BUTTON_HEIGHT;
    t->flags = D_EXIT;
    t->dp = (void *) "View IFFT";
    t->dp2 = (void *) this;
    t->dp3 = (void *) &SignalAnalyzerGameState::draw_ifft;
    n++;
    
    t = &dlg[n];
    t->proc = d_yield_proc;
    n++;
    
    int gui_fg_color = makecol(0,0,0);
    int gui_bg_color = makecol(200,240,200);
    set_dialog_color(dlg, gui_fg_color, gui_bg_color);
    dlg[0].bg = makecol(255, 255, 255); 
    dlg[1].bg = makecol(255, 255, 255);   
    position_dialog(dlg, 0, 0);
}

/*
 * Opens a file chooser dialog and reads the seledted WAV file into 
 * the data structure d.
 */
int SignalAnalyzerGameState::read_wav()
{
    riff_hdr riff;
    fmt_chunk fmt;
    data_chunk data;
    char *temp, fname[MAX_PATH];
    int i, j, offset, *sr, res;
    signed short int *q;
    FILE *in;
    
    res = file_select_ex("Choose a WAV file to analyze", fname, "wav;-h", 
        MAX_PATH, 400, 200);
    
    if (!res)
        return D_O_K;
    
    if ((in = fopen(fname, "r")) == NULL)
    {
        sprintf(status, "Status: Failed to load %s", fname);
        render();
        return D_O_K;
    }
    else
    {
        sprintf(status, "Status: Successfully loaded %s", fname);
        render();
    }
    
    fread(&riff, 12, 1, in);
    fread(&fmt, 24, 1, in);
    
    /* Skip the chunks we don't care about. */
    while (true)
    {
        fread(&data, sizeof(data_chunk), 1, in);
        
        if (strncmp(&data.sig[0], "data", 4) == 0)
        {
            break;
        }
        else
        {
            offset = (data.len[3] << 24) + (data.len[2] << 16) +
                (data.len[1] << 8) + data.len[0];
            fseek(in, offset, SEEK_CUR);
        }
    }

    /* Now riff, fmt, and data give us information about the WAV file. */    
    sr = (int *) &fmt.sample_rate;
    sample_rate = *sr;
    
    /* Read in the data we care about. */
    temp = (char *) calloc(NUM_SAMPLES * 2, sizeof(char));    
    fread(temp, sizeof(char), NUM_SAMPLES * 2, in);
    
    /* Convert Little Endian words to floats. */    
    for (i = 1, j = 0; i <= NUM_SAMPLES; i++, j += 2)
    {
        q = (signed short int *) &temp[j];
        
        this->sig[i] = (float) *q;
        this->d[i] = (float) *q;
    }
    
    /* Clean up. */
    free(temp);
    fclose(in);
    contents = CONTENTS_RAWDATA;

    window(sig);
    window(d);
    
    return D_O_K;
}

/*
 * Plots the raw data with gnuplot.
 */
int SignalAnalyzerGameState::draw_signal()
{
    FILE *f, *p;
    int i;
    
    if (contents == CONTENTS_NONE)
        return D_O_K;
    
    /* Write all the data to an output file. */
    f = fopen("plot.data", "w");    
    
    for (i = 1; i <= NUM_SAMPLES; i++)
    {
        fprintf(f, "%i %f\n", i, sig[i]);
    }
    
    fclose(f);
    
    /* Pipe the configuration file to gnuplot. */
    p = popen("gnuplot plot_sine.cfg", "w");
    pclose(p);
    
    /* Load the gnuplot output into a bitmap. */
    bmp = load_bitmap("plot.png", NULL);
    
    /* Render everything */
    render();
    
    return D_O_K;
}

/*
 * Plots the FFT data with gnuplot.
 */
int SignalAnalyzerGameState::draw_fft()
{
    FILE *f, *p;
    int i, j;
    
    if (contents == CONTENTS_NONE)
        return D_O_K;
        
    if (contents == CONTENTS_RAWDATA || contents == CONTENTS_IFFT)
    {
        fft(d, NUM_SAMPLES / 2, 1);
        contents = CONTENTS_FFT;
    }
    
    /* Write all the data to an output file. */
    f = fopen("plot.data", "w");
    
    for (i = 1, j = 0; i <= (NUM_SAMPLES / 2); i += 2, j++)
    {
        fprintf(f, "%f %f\n", j * (float) sample_rate / 
            ((float) NUM_SAMPLES / 2), 
            sqrt(d[i] * d[i] + d[i+1] * d[i+1]));
    }
    
    fclose(f);
    
    /* Pipe the configuration file to gnuplot. */
    p = popen("gnuplot plot_fft.cfg", "w");
    pclose(p);
    
    /* Load the gnuplot output into a bitmap. */
    bmp = load_bitmap("plot.png", NULL);
    
    /* Render everything */
    render();
    
    return D_O_K;
}

/*
 * Performs the highpass filter.
 */
int SignalAnalyzerGameState::highpass()
{
    if (contents == CONTENTS_NONE)
        return D_O_K;
    
    if (contents == CONTENTS_RAWDATA || contents == CONTENTS_IFFT)
    {
        fft(d, NUM_SAMPLES / 2, 1);
        contents = CONTENTS_FFT;
    }
    
    /* Perform an FFT of the signal. */
    highpass_filter(sig, d);
    
    return D_O_K;
}

/*
 * Plots the FFT data with gnuplot.
 */
int SignalAnalyzerGameState::draw_ifft()
{
    FILE *f, *p;
    int i;
    
    if (contents == CONTENTS_NONE)
        return D_O_K;
        
    if (contents == CONTENTS_FFT)
    {
        fft(d, NUM_SAMPLES / 2, -1);
        contents = CONTENTS_IFFT;
    }
    else if (contents == CONTENTS_RAWDATA)
    {
        fft(d, NUM_SAMPLES / 2, 1);
        fft(d, NUM_SAMPLES / 2, -1);
        contents = CONTENTS_IFFT;
    }
    
    /* Write all the data to an output file. */
    f = fopen("plot.data", "w");    
    
    for (i = 1; i <= NUM_SAMPLES; i++)
    {
        fprintf(f, "%i %f\n", i, d[i]);
    }
    
    fclose(f);
    
    /* Pipe the configuration file to gnuplot. */
    p = popen("gnuplot plot_ifft.cfg", "w");
    pclose(p);
    
    /* Load the gnuplot output into a bitmap. */
    bmp = load_bitmap("plot.png", NULL);
    
    /* Render everything */
    render();
    
    return D_O_K;
}

/*
 * Custom button proc for use in the SignalAnalyzerGameState menu.
 * Pointer to member function is supplied through dp3 and the "this" 
 * pointer is supplied through dp2. The function must take no other 
 * arguments and must return an int.
 */
int SignalAnalyzerGameState::mem_fun_button_proc(int msg, DIALOG *d, int c)
{
    int ret = d_button_proc(msg, d, c);
   
    if (ret == D_CLOSE && d->dp3)
        return ((int (*)(void *))d->dp3)(d->dp2);

    return ret;
}

/* 
 * Performs a highpass filter on an input array of data and stores it
 * in a separate array. Returns nothing.
 * 
 * data: Array of data points (one dimension)
 * out: Output array
 */
void SignalAnalyzerGameState::highpass_filter(float *data, float *out)
{
    /* TODO: Write this function. */
}

/*
 * Performs an FFT or inverse FFT on an input array of data.
 * 
 * data: Array of data points (one dimension)
 * nn: Number of complex data points, i.e. half the length of data
 * isign: If 1, does FFT; if -1, does inverse FFT
 * 
 * Notes: At the end of the inverse FFT, each element of the data array 
 *     will contain a single data point. At the end of the FFT, each 
 *     two consecutive elements of the array will contain the real 
 *     and imaginary parts, respectfully, of a single data point.
 * 
 * (C) Numerical Recipes in Fortran 77: The Art of Scientific Computing, 1986-1992
 */
void SignalAnalyzerGameState::fft(float *data, unsigned long nn, int isign)
{
    /* TODO: Write this function. */
}

/*
 * Applies some windowing function to the data array `d`.
 */
void SignalAnalyzerGameState::window(float *d)
{
	/* TODO: Write this function (hard points). */
}
